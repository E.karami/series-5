package sbu.cs;
import sbu.cs.magicMachine.*;
public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        Bloc[][] blocs=new Bloc[n][n];
        for (int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if( i==0&&j==0){
                    blocs[i][j]=new GreenBloc(input,arr[i][j]);
                }else if(i==0&&j<n-1){
                    GreenBloc greenBloc=(GreenBloc)blocs[i][j-1];
                    blocs[i][j]=new GreenBloc(greenBloc.getOutput(),arr[i][j]);
                }else if(j==0&&i<n-1){
                    GreenBloc greenBloc=(GreenBloc)blocs[i-1][j];
                    blocs[i][j]=new GreenBloc(greenBloc.getOutput(),arr[i][j]);
                }else if(i==0&&j==n-1){
                    GreenBloc greenBloc=(GreenBloc)blocs[i][j-1];
                    blocs[i][j]=new YellowBloc(greenBloc.getOutput(),arr[i][j]);
                }else if(j==0&&i==n-1){
                    GreenBloc greenBloc=(GreenBloc)blocs[i-1][j];
                    blocs[i][j]=new YellowBloc(greenBloc.getOutput(),arr[i][j]);
                }else if(i>0&&j>0&&i<n-1&&j<n-1){
                    if(i==1&&j==1){
                        GreenBloc left=(GreenBloc)blocs[1][0];
                        GreenBloc up=(GreenBloc)blocs[0][1];
                        blocs[i][j]=new BlueBloc(up.getOutput(),left.getOutput(),arr[i][j]);
                    }else if(j==1){
                        GreenBloc greenBloc=(GreenBloc)blocs[i][0];
                        BlueBloc blueBloc=(BlueBloc)blocs[i-1][j];
                        blocs[i][j]=new BlueBloc(blueBloc.getDownOutput(),greenBloc.getOutput(),arr[i][j]);
                    }else if(i==1){
                        GreenBloc greenBloc=(GreenBloc)blocs[0][j];
                        BlueBloc blueBloc=(BlueBloc)blocs[i][j-1];
                        blocs[i][j]=new BlueBloc(greenBloc.getOutput(),blueBloc.getRightOutput(),arr[i][j]);
                    }else{
                        BlueBloc up=(BlueBloc) blocs[i-1][j];
                        BlueBloc left=(BlueBloc)blocs[i][j-1];
                        blocs[i][j]=new BlueBloc(up.getDownOutput(),left.getRightOutput(),arr[i][j]);
                    }
                }else if(j==n-1||i==n-1){
                    if(i==n-1&&j==1){
                        YellowBloc yellowBloc=(YellowBloc)blocs[n-1][0];
                        BlueBloc blueBloc=(BlueBloc)blocs[i-1][1];
                        blocs[i][j]=new PinkBloc(blueBloc.getDownOutput(),yellowBloc.getOutput(),arr[i][j]);
                    }else if(i==1&&j==n-1){
                        YellowBloc yellowBloc=(YellowBloc)blocs[0][n-1];
                        BlueBloc blueBloc=(BlueBloc)blocs[1][j-1];
                        blocs[i][j]=new PinkBloc(yellowBloc.getOutput(),blueBloc.getRightOutput(),arr[i][j]);
                    }else if(i==n-1&&j<n-1){
                        PinkBloc pinkBloc=(PinkBloc)blocs[i][j-1];
                        BlueBloc blueBloc=(BlueBloc)blocs[i-1][j];
                        blocs[i][j]=new PinkBloc(blueBloc.getDownOutput(),pinkBloc.getOutput(),arr[i][j]);
                    }else if(j==n-1&&i<n-1){
                        PinkBloc pinkBloc=(PinkBloc)blocs[i-1][j];
                        BlueBloc blueBloc=(BlueBloc)blocs[i][j-1];
                        blocs[i][j]=new PinkBloc(pinkBloc.getOutput(),blueBloc.getRightOutput(),arr[i][j]);
                    }else{
                        PinkBloc up=(PinkBloc)blocs[i-1][j];
                        PinkBloc left=(PinkBloc)blocs[i][j-1];
                        blocs[i][j]=new PinkBloc(up.getOutput(),left.getOutput(),arr[i][j]);
                    }
                }
            }
        }
        PinkBloc pinkBloc=(PinkBloc)blocs[n-1][n-1];
        return pinkBloc.getOutput();
    }
}
