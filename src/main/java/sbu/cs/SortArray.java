package sbu.cs;

import java.util.Arrays;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for(int i=0;i<size;i++){
            int minimum_index=i;
            for(int j=i+1;j<size;j++){
                if(arr[j]<arr[minimum_index]){
                    minimum_index=j;
                }
            }
            swap(arr,i,minimum_index);
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for(int i=1;i<size;i++){
            int key=arr[i];
            int j=i-1;
            while(j>=0&&arr[j]>key){
                arr[j+1]=arr[j];
                j--;
            }
            arr[j+1]=key;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if(size>1){
            int middle=(size)/2;
            int[] part1=new int[middle];
            int[] part2=new int[size-middle];
            System.arraycopy(arr,0,part1,0,part1.length);
            System.arraycopy(arr,part1.length,part2,0,part2.length);
            return merge(mergeSort(part1,part1.length),mergeSort(part2,part2.length));
        }else{
            return arr;
        }
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int end=arr.length-1;
        int start=0;
        int middle=arr.length/2;
        while(start<=end){
            if(arr[middle]==value){
                return middle;
            }else if(arr[middle]>value){
                end=middle-1;
                middle=(end+start)/2;
            }else{
                start=middle+1;
                middle=(end+start)/2;
            }
        }
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        if(arr.length==0){
            return -1;
        }
        int middle=arr.length/2;
        if(arr[middle]==value){
            return middle;
        }else{
            if(arr[middle]>value){
                int[] subArray=Arrays.copyOfRange(arr,0,middle);
                return binarySearchRecursive(subArray,value);
            }else{
                int[] subArray=Arrays.copyOfRange(arr,middle+1,arr.length);
                int returned=binarySearchRecursive(subArray,value);
                if(returned!=-1){
                    returned+=middle+1;
                }
                return returned;
            }
        }
    }

    /**
     * this function swaps two element of an array
     * @param arr input array
     * @param first_index index of first element
     * @param second_index index of second element
     */
    private void swap(int[] arr,int first_index,int second_index){
        int temp=arr[first_index];
        arr[first_index]=arr[second_index];
        arr[second_index]=temp;
    }

    /**
     * this function gets two Ascending arrays of numbers and sort and merges them into a one array
     * @param array1 first array
     * @param array2 second array
     * @return merged array
     */
    private int[] merge(int[] array1,int[] array2){
        int[] result=new int[array1.length+array2.length];
        int i=0;
        int j=0;
        int index=0;
        while(index<result.length){
            if(i==array1.length){
                result[index]=array2[j];
                j++;

            }else if(j==array2.length){
                result[index]=array1[i];
                i++;
            }else if(array1[i]<=array2[j]){
                result[index]=array1[i];
                i++;
            }else{
                result[index]=array2[j];
                j++;
            }
            index++;
        }
        return result;

    }
}
