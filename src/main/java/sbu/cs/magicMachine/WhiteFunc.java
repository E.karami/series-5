package sbu.cs.magicMachine;

public class WhiteFunc implements WhiteFunction {
    private int functionType;

    public WhiteFunc(int functionType) {
        if(functionType<1||functionType>5){
            throw new IllegalArgumentException("the input number must be an integer between 1 and 5...");
        }else{
            this.functionType = functionType;
        }
    }

    public int getFunctionType() {
        return functionType;
    }

    public void setFunctionType(int functionType) {
        this.functionType = functionType;
    }

    @Override
    public String func(String arg1, String arg2) {
        StringBuilder stringBuilder=new StringBuilder();
        switch (this.functionType){
            case 1:{
                int firstSize=arg1.length();
                int secondSize=arg2.length();
                int index=0;
                for (;index<firstSize&&index<secondSize;index++){
                    stringBuilder.append(arg1.charAt(index)).append(arg2.charAt(index));
                }
                if(firstSize==secondSize){
                    return stringBuilder.toString();
                }else if(firstSize>secondSize){
                    stringBuilder.append(arg1.substring(index));
                    return stringBuilder.toString();
                }else {
                    stringBuilder.append(arg2.substring(index));
                    return stringBuilder.toString();
                }
            }
            case 2:{
                stringBuilder.append(arg2);
                return arg1.concat(stringBuilder.reverse().toString());
            }
            case 3:{
                int firstSize=arg1.length();
                int secondSize=arg2.length();
                int index=0;
                for (;index<firstSize&&index<secondSize;index++){
                    stringBuilder.append(arg1.charAt(index)).append(arg2.charAt(secondSize-1-index));
                }
                if(firstSize==secondSize){
                    return stringBuilder.toString();
                }else if(firstSize>secondSize){
                    stringBuilder.append(arg1.substring(index));
                    return stringBuilder.toString();
                }else {
                    stringBuilder.append(arg2.substring(0,secondSize-index));
                    return stringBuilder.toString();
                }
            }
            case 4:{
                if(arg1.length()%2==0){
                    return arg1;
                }else {
                    return arg2;
                }
            }
            case 5:{
                int firstSize=arg1.length();
                int secondSize=arg2.length();
                int index=0;
                for (;index<firstSize&&index<secondSize;index++){
                    stringBuilder.append((char)((arg1.charAt(index)-97+arg2.charAt(index)-97)%26+97));
                }
                if(firstSize==secondSize){
                    return stringBuilder.toString();
                }else if(firstSize>secondSize){
                    stringBuilder.append(arg1.substring(index));
                    return stringBuilder.toString();
                }else {
                    stringBuilder.append(arg2.substring(index));
                    return stringBuilder.toString();
                }
            }
            default:{
                System.err.println("This function never reach here...");
            }
        }
        return null;
    }
}
