package sbu.cs.magicMachine;

public class BlueBloc extends Bloc {
    private String upInput;
    private String leftInput;
    private String downOutput;
    private String rightOutput;
    private final BlackFunc function;

    public BlueBloc(String upInput, String leftInput,int functionType) {
        this.upInput = upInput;
        this.leftInput = leftInput;
        this.function=new BlackFunc(functionType);
        this.downOutput=this.function.func(this.upInput);
        this.rightOutput=this.function.func(this.leftInput);
    }

    public void setUpInput(String upInput) {
        this.upInput = upInput;
    }

    public void setLeftInput(String leftInput) {
        this.leftInput = leftInput;
    }

    public String getDownOutput() {
        return downOutput;
    }

    public String getRightOutput() {
        return rightOutput;
    }

    public void run(){
        this.downOutput=this.function.func(this.upInput);
        this.rightOutput=this.function.func(this.leftInput);
    }
}
