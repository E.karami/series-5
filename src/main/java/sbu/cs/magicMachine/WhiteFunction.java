package sbu.cs.magicMachine;

public interface WhiteFunction {

    public String func(String arg1, String arg2);

}
