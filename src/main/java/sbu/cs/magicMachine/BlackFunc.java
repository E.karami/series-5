package sbu.cs.magicMachine;

public class BlackFunc implements BlackFunction {
    private int functionType;

    public BlackFunc(int functionType) {
        if(functionType<1||functionType>5){
            throw new IllegalArgumentException("the input number must be an integer between 1 and 5...");
        }else{
            this.functionType = functionType;
        }
    }

    public int getFunctionType() {
        return functionType;
    }

    public void setFunctionType(int functionType) {
        this.functionType = functionType;
    }

    @Override
    public String func(String arg) {
        StringBuilder stringBuilder=new StringBuilder(arg);
        switch (this.functionType){
            case 1:{
                return stringBuilder.reverse().toString();
            }
            case 2:{
                StringBuilder str=new StringBuilder();
                for(int i=0;i<stringBuilder.length();i++){
                    str.append(stringBuilder.charAt(i)).append(stringBuilder.charAt(i));
                }
                return str.toString();
            }
            case 3:{
                return stringBuilder.toString().concat(stringBuilder.toString());
            }
            case 4:{
                char end=stringBuilder.charAt(stringBuilder.length()-1);
                stringBuilder.deleteCharAt(stringBuilder.length()-1);
                return end+stringBuilder.toString();
            }
            case 5:{
                StringBuilder result=new StringBuilder();
                for(int i=0;i<stringBuilder.length();i++){
                    result.append((char)(219-stringBuilder.charAt(i)));
                }
                return result.toString();
            }
            default:{
                System.err.println("This function never reach here...");
            }
        }
        return null;
    }
}
