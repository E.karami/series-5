package sbu.cs.magicMachine;

public class YellowBloc extends Bloc{
    private String input;
    private String output;
    private final BlackFunc function;

    public YellowBloc(String input,int innerFunctionType) {
        this.input = input;
        this.function=new BlackFunc(innerFunctionType);
        this.output=this.function.func(this.input);
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void run(){
        this.output=this.function.func(this.input);
    }
}
