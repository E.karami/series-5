package sbu.cs.magicMachine;

public class PinkBloc extends Bloc{
    private String upInput;
    private String leftInput;
    private String output;
    private final WhiteFunc function;

    public PinkBloc(String upInput, String leftInput,int functionType) {
        this.upInput = upInput;
        this.leftInput = leftInput;
        this.function=new WhiteFunc(functionType);
        this.output=this.function.func(this.leftInput,this.upInput);
    }

    public String getOutput() {
        return output;
    }

    public void setUpInput(String upInput) {
        this.upInput = upInput;
    }

    public void setLeftInput(String leftInput) {
        this.leftInput = leftInput;
    }

    public void run(){
        this.output=this.function.func(this.leftInput,this.upInput);
    }
}
