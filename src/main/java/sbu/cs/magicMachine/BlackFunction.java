package sbu.cs.magicMachine;

public interface BlackFunction {

    public String func(String arg);

}
